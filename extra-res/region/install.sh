#!/usr/bin/env bash

KUBECTL=${KUBECTL:-"kubectl"}

APPRELEASE_NAME=${APPRELEASE_NAME:-"region"}
NAMESPACE=${NAMESPACE:-"alauda-system"}
REPLICAS=${REPLICAS:-"2"}
CHART_VERSION=${CHART_VERSION:-"unknown"}
CHART_REPO=${CHART_REPO:-"unknown"}
REGISTRY_ADDR=${REGISTRY_ADDR:-""}
GLOBAL_SCHEME=${GLOBAL_SCHEME:-"https"}
GLOBAL_HOST=${GLOBAL_HOST:-"unknown.domain"}
DEFAULT_ADMIN=${DEFAULT_ADMIN:-"admin"}
CLUSTER_NAME=${CLUSTER_NAME:-"business"}
ADMIN_TOKEN=${ADMIN_TOKEN:-"unknown"}
LABEL_BASE_DOMAIN=${LABEL_BASE_DOMAIN:-"alauda.io"}

function render() {
    local path=$1
    local v
    for k in APPRELEASE_NAME NAMESPACE REPLICAS CHART_VERSION CHART_REPO REGISTRY_ADDR GLOBAL_SCHEME GLOBAL_HOST DEFAULT_ADMIN CLUSTER_NAME ADMIN_TOKEN LABEL_BASE_DOMAIN; do
        eval v=\$${k}
        echo "$k=$v"
        sed -i "s|$k|$v|g" "${path}"
    done
}

${KUBECTL} create ns ${NAMESPACE}

path="/opt/region/region.yaml"
render "${path}"
${KUBECTL} apply -f "${path}"

#wait ready
JSONPATH="-o=jsonpath={.status.conditions[?(@.type=='Sync')].status}{','}{.status.conditions[?(@.type=='Health')].status}"

while true; do
    STATUS=$(${KUBECTL} get apprelease -n "${NAMESPACE}" ${APPRELEASE_NAME} ${JSONPATH})
    echo "AppRelease STATUS=${STATUS}"

    if [[ "${STATUS}" == "True,True" ]]; then
        echo "AppRelease is ready"
        exit 0
    fi

     if [[ "${STATUS}" == "False,False" ]]; then
        echo "AppRelease is failed"
        exit 1
    fi

    sleep 5
done
