#!/usr/bin/env bash

SLEEP_SECONDS=${SLEEP_SECONDS:-"5"}
JOB_NAME=${JOB_NAME:-"deploy-region"}
KUBECTL=${KUBECTL:-"kubectl"}
LOG_FILE=${LOG_FILE:-"/tmp/check-job.log"}

JSONPATH="-o=jsonpath={.status.conditions[?(@.type=='Complete')].status}{','}{.status.conditions[?(@.type=='Failed')].status}"

GET_JOB_ARGS=${GET_JOB_ARGS:-"get job -n kube-system ${JOB_NAME}"}

GET_NS_LABEL_ARGS=${GET_NS_LABEL_ARGS:-"get ns kube-system -o=jsonpath={.metadata.labels.region-component}"}

ADD_NS_LABEL_ARGS=${ADD_NS_LABEL_ARGS:-"label ns kube-system region-component=deployed"}

PID=$$
function log() {
    echo $(date +"%F %T.%N") ${PID} $1 >> ${LOG_FILE}
}

log "Check Job Start"

while true; do

    LABEL=$(${KUBECTL} ${GET_NS_LABEL_ARGS} 2>&1)
    if [[ "${LABEL}" == "deployed" ]]; then
        log "Component Installed!"
        exit 0
    fi

    STATUS=$(${KUBECTL} ${GET_JOB_ARGS} ${JSONPATH} 2>&1)
    log "STATUS=${STATUS}"
    
    if [[ "${STATUS}" == "True," ]]; then
        log "Job Succeed!"
        ${KUBECTL} ${ADD_NS_LABEL_ARGS}
        exit 0
    fi

    if [[ "${STATUS}" == ",True" ]]; then
        log "Job Failed!"
        exit 1
    fi

    sleep ${SLEEP_SECONDS}
done
