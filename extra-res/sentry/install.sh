#!/usr/bin/env bash

KUBECTL=${KUBECTL:-"kubectl"}

NAMESPACE=${NAMESPACE:-"alauda-system"}
REPLICAS=${REPLICAS:-"1"}
SENTRY_IMAGE=${SENTRY_IMAGE:-"alaudak8s/sentry:latest"}
SYNC_PERIOD=${SYNC_PERIOD:-"7200"}
WORKERS=${WORKERS:-"3"}

function render() {
    local path=$1
    local v
    for k in NAMESPACE REPLICAS SENTRY_IMAGE SYNC_PERIOD WORKERS; do
        eval v=\$${k}
        sed -i "s|$k|$v|g" ${path}
    done
}

${KUBECTL} create ns ${NAMESPACE}
for f in crd clusterrolebinding deploy service; do 
    path="/opt/sentry/${f}.yaml"
    render "${path}"
    ${KUBECTL} apply -f "${path}"
done

#wait ready

JSONPATH="-o=jsonpath={.status.conditions[?(@.type=='Available')].status}"

GET_DEPLOY_ARGS=${ARGS:-"get deploy -n ${NAMESPACE} sentry"}


while true; do
    STATUS=$(${KUBECTL} ${GET_DEPLOY_ARGS} ${JSONPATH} 2>&1)
    echo "sentry STATUS=${STATUS}"
    
    if [[ "${STATUS}" == "True" ]]; then
        echo "sentry is ready!"
        exit 0
    fi
    sleep 5
done
