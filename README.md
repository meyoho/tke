# 说明

- 本仓库包手tke的流水线配置文件、简版tke-installer的构建文件和extra-res的内容
- tke源码在 [https://github.com/tkestack/tke](https://github.com/tkestack/tke)，这里只有Dockerfile
- 构建tke源码需要创建 `acp-branch`-github-`github-branch`分支，期中`acp-tranch`是ACP的分支例如master, release-2.9等，构建成功后将更新相应的chart。`githb-branch`是github上tke的分支，将拉取对应的分支进行构建。`github-branch`是master才会构建用到的所有tke组件，否则只构建简版tke-installer。
- extra-res按正常的acp流水线打tag和release分支即可
