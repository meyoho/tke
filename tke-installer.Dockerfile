FROM alpine:3.10
ARG TARGETARCH

FROM tkestack/provider-res-${TARGETARCH:-amd64}:v1.16.6-4 AS provider

ADD . /tmp

RUN ARCH= && dpkgArch="$(arch)" \
    && case "${dpkgArch}" in \
        x86_64) ARCH='amd64';rm -rf /data/res/linux-arm64;; \
        aarch64) ARCH='arm64';rm -rf /data/res/linux-amd64;; \
        *) echo "unsupported architecture"; exit 1 ;; \
        esac \
    && cp /tmp/${ARCH}/tke-installer /

COPY provider/conf /data/conf
COPY provider/manifests /data/manifests

FROM alpine:3.10

WORKDIR /app

ENV PATH="/app/bin:$PATH"

RUN apk add --no-cache bash curl docker 

COPY --from=provider /tke-installer /app/bin/
COPY --from=provider /data /app/provider/baremetal/
COPY manifests /app/manifests
COPY hooks /app/hooks

 
RUN mkdir -p /app/conf && \
    mkdir -p /app/data && \
    mkdir -p /app/.docker && \
    ln -s /app/.docker /root/.docker

ENTRYPOINT ["/app/bin/tke-installer"]
