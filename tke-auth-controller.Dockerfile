FROM alpine:3.10 AS source

ADD . /tmp

RUN ARCH= && dpkgArch="$(arch)" \
    && case "${dpkgArch}" in \
        x86_64) ARCH='amd64';; \
        aarch64) ARCH='arm64';; \
        *) echo "unsupported architecture"; exit 1 ;; \
        esac \
    && cp /tmp/${ARCH}/tke-auth-controller /

FROM alpine:3.10

RUN echo "hosts: files dns" >> /etc/nsswitch.conf

WORKDIR /app
COPY --from=source /tke-auth-controller /app/bin/
COPY auth/ /app/auth

ENTRYPOINT ["/app/bin/tke-auth-controller"]
